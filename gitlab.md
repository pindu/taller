Git global setup
```
git config --global user.name "Luciano Parruccia"
git config --global user.email "parruccial@yahoo.com.ar"
```
Create a new repository
```
git clone git@gitlab.com:pindu/taller.git
cd taller
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master
```
Push an existing folder
```
cd existing_folder
git init
git remote add origin git@gitlab.com:pindu/taller.git
git add .
git commit -m "Initial commit"
git push -u origin master
```