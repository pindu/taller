Tutorial para instalar Django [Turorial de Django!](https://docs.djangoproject.com/en/2.0/intro/tutorial01/)
Tutorial para crear un entorno virtual de python [Tutorial para crear entorno virtual](https://docs.djangoproject.com/en/2.0/intro/contributing/)

[Repositorio con las fuentes](https://gitlab.com/pindu/taller)

# Pasos básicos para instalación en linux
### Creamos el entorno virtual en caso de querer instalar algunas herramientas de python, pero no es necesario, ya que vamos a trabajar con contenedores
```
mkdir ~/.virtualenvs
python3 -m venv ~/.virtualenvs/djangodev
```
### Generamos la app y reemplazamos **user** por el usuario con el que vamos a trabajar, para darle los permisos necesarios para que pueda modificarlos
```
docker-compose run generate
docker-compose run manage startapp turnos
sudo chown user:user -R .
```
### Realizamos la configuración del archivo **setting.py**
```
# Permitimos las conexión remota
ALLOWED_HOSTS = ['*']
CORS_ORIGIN_ALLOW_ALL = os.environ.get('APP_CORS_ALLOW_ALL','False') == 'True'
CORS_ORIGIN_WHITELIST = os.environ.get('ALLOWED_HOSTS','localhost:8000').split(',')

INSTALLED_APPS = [
    ...
    # agregamos nuestra app al final de la lista
    'turnos',
]
#Configuramos la base de datos
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql', 
            'NAME': 'postgres',                     
            'USER': 'postgres',
            'PASSWORD': 'sa',
            'HOST': 'db',                     
            'PORT': '5432',                    
    }
}
```
# Comandos útiles
### Migro la base de datos que se crea por defecto para el proyecto.
```
docker-compose run manage migrate
```
### Para generar un súper usuario de Django
```
docker-compose run manage createsuperuser
```
### Iniciamos los contenedores de la aplicación. El comando -d es para ejecutarlo como demonio, si queremos ir viendo el log de los contenedores, eliminamos el -d
```
docker-compose up app -d
```

### En caso de tener algún problema, podemos ver el log de los contenedores
```
docker logs -f taller_app_1
docker logs -f taller_db_1
```
### Para generar las modificaciones de la base de datos ejecutamos:
```
docker-compose run manage makemigrations
docker-compose run manage migrate
```
### Para el caso que deseamos eliminar todo lo generado por el docker-compose, siempre y cuando los contenedores no estén iniciados, ejecutamos:
```
docker system prune -a
```
### En el caso de tener problemas para modificar o eliminar algún archivo, le cambiamos el propietario con el siguiente comando, y reemplazamos **user** por el usuario con el que estamos logueado
```
sudo chown user:user -R .
```